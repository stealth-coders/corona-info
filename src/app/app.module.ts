import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HeaderComponent } from "./components/header/header/header.component";
import { IndexPageComponent } from "./components/pages/index/index-page/index-page.component";
import { AboutPageComponent } from "./components/pages/about/about-page/about-page.component";
import { RegisterPageComponent } from "./components/pages/register/register-page/register-page.component";
import { IndexCardComponent } from "./components/pages/index/index-card/index-card.component";
import { RegisterTagComponent } from "./components/pages/register/register-tag/register-tag.component";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { HashLocationStrategy, LocationStrategy } from "@angular/common";
import { AbbrevationPipe } from "./Abbrevation.pipe";
import { RECAPTCHA_V3_SITE_KEY, RecaptchaV3Module } from "ng-recaptcha";
import { FooterComponent } from "./components/footer/footer.component";
import { ImpressumPageComponent } from "./components/pages/info/impressum-page/impressum-page.component";
import { DataprivacyPageComponent } from "./components/pages/info/dataprivacy-page/dataprivacy-page.component";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    IndexPageComponent,
    AboutPageComponent,
    RegisterPageComponent,
    ImpressumPageComponent,
    DataprivacyPageComponent,
    IndexCardComponent,
    RegisterTagComponent,
    AbbrevationPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    FontAwesomeModule,
    BrowserAnimationsModule,
    RecaptchaV3Module
  ],
  providers: [
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    {
      provide: RECAPTCHA_V3_SITE_KEY,
      useValue: "6Les2-QUAAAAAMlwT_Mtt1LsOOEhet2FMXCTdn29"
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}

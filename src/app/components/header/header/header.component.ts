import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { faBars } from '@fortawesome/free-solid-svg-icons'
import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"],
  animations: [
    trigger('toggle', [
      state('open', style({ height: '*', 'padding-bottom': '8px' })),
      state('closed', style({ height: '0px', 'padding-bottom': '0px' })),
      transition('open <=> closed', animate('200ms ease-in-out'))
    ])
  ],
})
export class HeaderComponent implements OnInit {
  faBars = faBars;
  href;
  constructor(private router: Router) {
    router.events.subscribe(val => {
      this.href = router.url;
    });
  }

  ngOnInit() {
    this.href = this.router.url;
  }

  onclick(url) {
    this.router.navigateByUrl(url);
    this.state = "closed";
  }

  state = "closed";
  toggleNav() {
    this.state = (this.state == "open") ? "closed" : "open";
  }
}

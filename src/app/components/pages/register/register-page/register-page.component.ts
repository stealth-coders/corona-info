import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Observable } from "rxjs";
import { $ } from "protractor";
import { HttpClient } from "@angular/common/http";
import { RefElement, Tag } from "../../index/index-page/index-page.component";
import { OnExecuteData, ReCaptchaV3Service } from "ng-recaptcha";
import { Subscription } from "rxjs";

@Component({
  selector: "app-register-page",
  templateUrl: "./register-page.component.html",
  styleUrls: ["./register-page.component.css", "../../pages.css"]
})
export class RegisterPageComponent implements OnInit {
  descCount: number = 0;
  descMax: number = 200;
  nameCount: number = 0;
  nameMax: number = 20;
  tags: Tag[];
  hover: boolean;
  error: string;
  showError: boolean;
  showSucc: boolean = false;
  checkboxes = {
    dataPrivacy: false,
    website: false,
    android: false,
    ios: false
  };
  tags$: Observable<Tag[]>;

  currentEntry: RefElement = <RefElement>{};
  nameOfOwner: string;
  emailOfOwner: string;

  private allExecutionsSubscription: Subscription;
  private singleExecutionSubscription: Subscription;
  public recentToken: string = "";
  public readonly executionLog: OnExecuteData[] = [];

  constructor(
    private http: HttpClient,
    private recaptchaV3Service: ReCaptchaV3Service
  ) {}

  ngOnInit() {
    this.allExecutionsSubscription = this.recaptchaV3Service.onExecute.subscribe(
      data => this.executionLog.push(data)
    );

    this.tags$ = this.http.get<Tag[]>(
      "https://api.wie-kann-ich-helfen.eu/v1/tag"
    );
    this.tags$.subscribe(data => {
      this.tags = data;
      for (const tag of this.tags) {
        tag["selected"] = false;
      }
    });
    this.currentEntry.category = "hilfe";
    this.currentEntry.language = "german";
    this.showError = false;
  }

  captchaV3() {
    var succ = this.checkFormData();
    if (!succ) {
      this.showError = true;
      return;
    } else {
      this.showError = false;
    }
    if (this.singleExecutionSubscription) {
      this.singleExecutionSubscription.unsubscribe();
    }
    this.singleExecutionSubscription = this.recaptchaV3Service
      .execute("submitForm")
      .subscribe(token => {
        var mother = {};
        mother["token"] = token;
        this.http
          .post<any>("https://api.wie-kann-ich-helfen.eu/v1/recaptcha", mother)
          .subscribe(data => {
            if (data.success && data.score > 0.7) this.submitForm();
          });
      });
  }

  getCurrentType() {
    var c = this.checkboxes;

    if (c.ios && c.android && c.website) return "app_both_website";
    if (c.ios && c.android) return "app_both";
    if (c.ios && c.website) return "app_ios_website";
    if (c.android && c.website) return "app_android_website";
    if (c.ios) return "app_ios";
    if (c.android) return "app_android";
    if (c.website) return "website";
    return null;
  }

  selectedTags() {
    if (this.tags == null) return [];
    return this.tags.filter(element => element["selected"]);
  }

  submitForm() {
    var mother = {};
    mother["name"] = this.currentEntry.name;
    mother["description"] = this.currentEntry.description;
    mother["link"] = this.currentEntry.link;
    mother["type"] = this.getCurrentType();
    if (
      this.currentEntry.youtubeLink != null &&
      this.currentEntry.youtubeLink.trim() != ""
    ) {
      var videoId = this.currentEntry.youtubeLink.substring(
        this.currentEntry.youtubeLink.lastIndexOf("=") + 1,
        this.currentEntry.youtubeLink.length
      );
      mother["youtubeLink"] = videoId;
    } else {
      mother["youtubeLink"] = "";
    }
    mother["category"] = this.currentEntry.category;
    mother["ownerName"] = this.nameOfOwner;
    mother["ownerEmail"] = this.emailOfOwner;
    mother["language"] = this.currentEntry.language;
    var tags = [];
    for (const tag of this.tags) {
      if (tag["selected"]) tags.push(tag["id"]);
    }
    mother["tags"] = tags;

    this.http
      .post<any>("https://api.wie-kann-ich-helfen.eu/v1/refelement", mother)
      .subscribe(data => {
        if (data.message == "success") {
          this.showError = false;
          this.showSucc = true;
          this.currentEntry = <RefElement>{};
          this.currentEntry.category = "hilfe";
          this.currentEntry.language = "german";
          this.nameOfOwner = "";
          this.emailOfOwner = "";
          this.checkboxes = {
            dataPrivacy: false,
            website: false,
            android: false,
            ios: false
          };
          for (const tag of this.tags) {
            tag["selected"] = false;
          }
        }
      });
  }

  public ngOnDestroy() {
    if (this.allExecutionsSubscription) {
      this.allExecutionsSubscription.unsubscribe();
    }
    if (this.singleExecutionSubscription) {
      this.singleExecutionSubscription.unsubscribe();
    }
  }

  checkFormData() {
    var refName = this.currentEntry["name"];
    var refDesc = this.currentEntry["description"];
    var category = this.currentEntry["category"];
    var type = this.getCurrentType();
    var link = this.currentEntry["link"];
    var ownerName = this.nameOfOwner;
    var ownerEmail = this.emailOfOwner;
    if (refName == null || refName.trim() == "") {
      this.setError("Name des Services darf nicht leer sein.");
      return false;
    }
    if (refDesc == null || refDesc.trim() == "" || refDesc.length < 120) {
      this.setError(
        "Beschreibung des Services darf nicht kürzer als 120 Zeichen sein."
      );
      return false;
    }
    if (category == null || category.trim() == "") {
      this.setError("Kategorie des Services darf nicht leer sein.");
      return false;
    }
    if (type == null || type.trim() == "") {
      this.setError("Du musst mindestens eine Plattform anklicken.");
      return false;
    }
    if (
      link == null ||
      link.trim() == "" ||
      !(link.startsWith("http://") || link.startsWith("https://"))
    ) {
      this.setError(
        "Link zur Website/App darf nicht leer sein. / Fängt nicht mit http(s):// an."
      );
      return false;
    }
    if (ownerName == null || ownerName.trim() == "") {
      this.setError("Name des Betreibers darf nicht leer sein.");
      return false;
    }
    if (
      ownerEmail == null ||
      ownerEmail.trim() == "" ||
      !ownerEmail.includes("@")
    ) {
      this.setError("Ungültige E-Mail Adresse.");
      return false;
    }
    if (
      ownerEmail == null ||
      ownerEmail.trim() == "" ||
      !ownerEmail.includes("@")
    ) {
      this.setError("Ungültige E-Mail Adresse.");
      return false;
    }
    if (!this.checkboxes.dataPrivacy) {
      this.setError("Du musst die angegebenen Angaben bestätigen.");
      return false;
    }
    var tags = [];
    for (const tag of this.tags) {
      if (tag["selected"]) tags.push(tag["id"]);
    }
    if (tags.length == 0) {
      this.setError("Du musst mindestens einen Tag auswählen.");
      return false;
    }
    if (tags.length > 8) {
      this.setError("Du darfst maximal 8 Tags auswählen.");
      return false;
    }
    return true;
  }

  setError(error: string) {
    this.error = error;
  }

  onKey(value, root) {
    if (root == "name") {
      this.nameCount = value.length;
    } else if (root == "desc") this.descCount = value.length;
  }
}

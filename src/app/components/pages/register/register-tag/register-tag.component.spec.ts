import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterTagComponent } from './register-tag.component';

describe('RegisterTagComponent', () => {
  let component: RegisterTagComponent;
  let fixture: ComponentFixture<RegisterTagComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterTagComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterTagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

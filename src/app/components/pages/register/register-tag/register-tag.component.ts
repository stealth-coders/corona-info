import {
  Component,
  OnInit,
  Input,
  ChangeDetectorRef,
  ApplicationRef,
  NgZone
} from "@angular/core";
import { Tag, invertColorCode } from '../../index/index-page/index-page.component';
@Component({
  selector: "app-register-tag",
  templateUrl: "./register-tag.component.html",
  styleUrls: ["./register-tag.component.css"]
})
export class RegisterTagComponent implements OnInit {
  @Input() content: Tag;

  constructor() { }

  ngOnInit() { }

  click() {
    this.content["selected"] = !this.content["selected"];
  }

  invertColor(hex) {
    return invertColorCode(hex);
  }
}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-impressum-page',
  templateUrl: './impressum-page.component.html',
  styleUrls: ['../info-pages.css']
})
export class ImpressumPageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

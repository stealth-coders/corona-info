import { Component, OnInit } from "@angular/core";
import { ReCaptchaV3Service } from 'ng-recaptcha';

@Component({
  selector: "app-about-page",
  templateUrl: "./about-page.component.html",
  styleUrls: ["./about-page.component.css", "../../pages.css"]
})
export class AboutPageComponent implements OnInit {
  constructor(private recaptchaV3Service: ReCaptchaV3Service) {}

  ngOnInit() {}
}

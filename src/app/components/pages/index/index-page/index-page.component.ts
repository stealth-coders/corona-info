import { Component, OnInit, ViewChildren, QueryList } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { $, element } from "protractor";
import { IndexCardComponent } from "../index-card/index-card.component";
import { ReCaptchaV3Service } from "ng-recaptcha";
import { faDesktop } from "@fortawesome/free-solid-svg-icons";
import { faApple } from "@fortawesome/free-brands-svg-icons";
import {
  trigger,
  state,
  style,
  animate,
  transition
} from "@angular/animations";

export interface Tag {
  id: number;
  name: string;
  description: string;
  color: string;
}
export interface RefElement {
  id: number;
  name: string;
  description: string;
  category: string;
  link: string;
  youtubeLink: string;
  type: string;
  image: string;
  language: string;
  tags: Tag[];
}

export function invertColorCode(hex) {
  if (hex.indexOf("#") === 0) {
    hex = hex.slice(1);
  }
  // convert 3-digit hex to 6-digits.
  if (hex.length === 3) {
    hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
  }
  if (hex.length !== 6) {
    throw new Error("Invalid HEX color.");
  }
  // invert color components
  var r = (255 - parseInt(hex.slice(0, 2), 16)).toString(16),
    g = (255 - parseInt(hex.slice(2, 4), 16)).toString(16),
    b = (255 - parseInt(hex.slice(4, 6), 16)).toString(16);

  // pad each with zeros and return
  return "#" + padZero(r) + padZero(g) + padZero(b);
}

export function padZero(str, len = 2) {
  var zeros = new Array(len).join("0");
  return (zeros + str).slice(-len);
}

@Component({
  selector: "app-index-page",
  templateUrl: "./index-page.component.html",
  styleUrls: ["./index-page.component.css", "../../pages.css"],
  animations: [
    trigger("toggleBackground", [
      state("fadein", style({ opacity: "1", visibility: "visible" })),
      state("fadeout", style({ opacity: "0", visibility: "hidden" })),
      transition("fadein <=> fadeout", animate("200ms ease-in-out"))
    ])
  ]
})
export class IndexPageComponent implements OnInit {
  faDesktop = faDesktop;
  faApple = faApple;

  animationState = "fadeout";

  finalData;
  refElements;
  tagElements;

  categories = [];
  selectedTags = [];
  categoriesToElements = {};
  categoriesToTags = {};

  videoShown: boolean = false;

  @ViewChildren(IndexCardComponent) private cardComponents: QueryList<
    IndexCardComponent
  >;

  //True wenn eine Karte im Videomodus is
  cardOpened = false;

  elements$: Observable<RefElement[]>;
  tags$: Observable<Tag[]>;

  constructor(
    private http: HttpClient,
    private recaptchaV3Service: ReCaptchaV3Service
  ) {
    this.http = http;
  }

  ngOnInit() {
    this.elements$ = this.http.get<RefElement[]>(
      "https://api.wie-kann-ich-helfen.eu/v1/refelement"
    );
    this.tags$ = this.http.get<Tag[]>(
      "https://api.wie-kann-ich-helfen.eu/v1/tag"
    );
    this.elements$.subscribe(data => {
      this.refElements = data;
      this.finalData = data;

      for (const refElement of data) {
        refElement["visible"] = true;
        if (this.categoriesToElements[refElement.category] != null) {
          this.categoriesToElements[refElement.category].push(refElement);
        } else {
          this.categoriesToElements[refElement.category] = [refElement];
          this.categories.push(refElement.category);
        }
      }
    });

    this.tags$.subscribe(data => {
      this.tagElements = data;
      //  this.selectedTags = data.map(element => element.name);
    });
  }
  noResults = false;
  onKey(value: string) {
    this.noResults = true;
    if (value == "") {
      for (const element of this.refElements) {
        if (this.selectedTags.length == 0) {
          this.noResults = false;
          element.visible = true;
        } else {
          var found = false;
          for (const selectedTag of this.selectedTags) {
            if (element.tags.map(el => el.name).includes(selectedTag)) {
              this.noResults = false;
              found = true;
            }
          }
          element.visible = found;
        }
      }
    } else {
      for (const category of this.categories) {
        for (const element of this.categoriesToElements[category]) {
          // ELement has one of the selected tags

          if (this.selectedTags.length == 0) {
            element["visible"] = this.elementContainsSearch(element, value);
          } else {
            var found = false;
            for (const selectedTag of this.selectedTags) {
              if (element.tags.map(el => el.name).includes(selectedTag)) {
                found = true;
                break;
              }
            }
            if (found && this.elementContainsSearch(element, value)) {
              this.noResults = false;
              element["visible"] = true;
            }
          }
        }
      }
    }
  }

  elementContainsSearch(element, search) {
    var toCompare = [];
    toCompare.push(element.category);
    toCompare = toCompare.concat(element.name.toLowerCase().split(" "));
    toCompare = toCompare.concat(element.description.toLowerCase().split(" "));

    var searchedWords = search.toLowerCase().split(" ");

    for (const element of toCompare) {
      for (const searchWord of searchedWords) {
        if (element.indexOf(searchWord) !== -1) return true;
      }
    }
    return false;
  }

  getCardsFor(cat) {
    return this.categoriesToElements[cat].filter(element => element.visible);
  }

  getTags() {
    return this.tagElements;
  }

  onTagClick(tag) {
    if (!this.selectedTags.includes(tag)) this.selectedTags.push(tag);
    else this.selectedTags.splice(this.selectedTags.indexOf(tag), 1);
    this.onKey(document.getElementById("search")["value"]);
  }

  invertColor(hex) {
    return invertColorCode(hex);
  }

  lastVideoOpened = -1;

  forceClose() {
    this.changeOpenState(-2);
  }

  changeOpenState(id) {
    this.videoShown = !this.videoShown;
    if (this.videoShown) {
      this.animationState = "fadein";
    } else {
      this.animationState = "fadeout";
    }
    if (id == -1) return;

    this.lastVideoOpened = id;
    var allCards = this.cardComponents.toArray();
    for (var x = 0; x < allCards.length; x++) {
      allCards[x].forceClose(id);
    }
  }
}

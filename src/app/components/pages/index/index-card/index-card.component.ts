import {
  Component,
  OnInit,
  Input,
  ChangeDetectorRef,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
  HostListener
} from "@angular/core";
import { faDesktop } from "@fortawesome/free-solid-svg-icons";
import { faApple } from "@fortawesome/free-brands-svg-icons";
import {
  RefElement,
  invertColorCode
} from "../index-page/index-page.component";
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";
import { ThrowStmt } from "@angular/compiler";

@Component({
  selector: "app-index-card",
  templateUrl: "./index-card.component.html",
  styleUrls: ["./index-card.component.css"],
  host: {
    "[style.flex-grow]": "flexGrow",
    "[style.max-width]": "maxWidth",
    "[style.z-index]": "zIndex",
    "[style.position]": "hPosition"
  }
})
export class IndexCardComponent implements OnInit {
  faDesktop = faDesktop;
  faApple = faApple;

  @Input() content: RefElement;
  @ViewChild("contentElement", null) input: ElementRef;
  @ViewChild("more", null) moreButton: ElementRef;
  @ViewChild("tags", null) tags: ElementRef;
  @ViewChild("videoButton", null) videoButton: ElementRef;
  @ViewChild("videoScreen", null) videoScreen: ElementRef;
  @ViewChild("contentElement", null) contentElement: ElementRef;
  showTagNames = false;
  flexGrow: string;
  maxWidth: string;
  showVideo = false;
  hasVideo = false;
  youtubeLinkSafe: SafeHtml;
  screenHeight;
  screenWidth;

  zIndex = 1;
  hPosition = "initial";

  @Output() onOpen = new EventEmitter<number>();

  constructor(
    private cd: ChangeDetectorRef,
    private hostElement: ElementRef,
    private sanitizer: DomSanitizer
  ) {
    this.onResize();
  }

  @HostListener("window:resize", ["$event"])
  onResize(event?) {
    this.screenHeight = window.innerHeight;
    this.screenWidth = window.innerWidth;

    this.showTagNames = this.screenWidth <= 1000;
  }

  ngOnInit() {
    const iframe = this.hostElement.nativeElement.querySelector("iframe");
    if (iframe.src == null || iframe.src.trim() == "")
      iframe.src = "https://www.youtube.com/embed/" + this.content.youtubeLink;
    this.hasVideo =
      this.content.youtubeLink != null && this.content.youtubeLink.trim() != "";
    if (window.innerWidth < 1000) {
      this.showTagNames = true;
    }
  }

  onMore() {
    window.open(this.content.link, "_blank");
  }

  toggleVideo() {
    if (!this.showVideo) {
      this.onOpen.emit(this.content.id);
    } else {
      this.onOpen.emit(-1);
    }
    this.showVideo = !this.showVideo;
    if (!this.showVideo) {
      this.zIndex = 1;
      this.hPosition = "initial";

      this.input.nativeElement.style.animation =
        "collapseContent 0.3s 0s 1 forwards";
      this.tags.nativeElement.style.animation =
        "collapseContentChild 0.2s 0.1s 1 forwards";

      //Playback stops
      this.videoScreen.nativeElement.src = "";
      this.videoScreen.nativeElement.src =
        "https://www.youtube.com/embed/" + this.content.youtubeLink;
    } else {
      if (this.screenWidth > 1000) {
        this.moreButton.nativeElement.style.top = "-20px";
        this.videoButton.nativeElement.style.top = "-20px";
      }
      this.zIndex = 9999;
      this.hPosition = "static";
      if (this.screenHeight <= 1000) {
        this.contentElement.nativeElement.style.maxHeight = "300px";
        this.contentElement.nativeElement.style.minHeight = "300px";
      }
      this.tags.nativeElement.style.visibility = "hidden";
      this.input.nativeElement.style.animation = "none";
      this.tags.nativeElement.style.animation = "none";
    }
  }

  forceClose(id) {
    if (this.showVideo && this.content.id != id) {
      this.zIndex = 1;
      this.hPosition = "initial";
      this.showVideo = false;
      this.toggleTagNames(false);

      this.input.nativeElement.style.animation =
        "collapseContent 0.3s 0s 1 forwards";

      this.tags.nativeElement.style.animation =
        "collapseContentChild 0.2s 0.1s 1 forwards";
      this.videoButton.nativeElement.style.transition = "none";

      //Playback stops
      this.videoScreen.nativeElement.src =
        "https://www.youtube.com/embed/" + this.content.youtubeLink;
    }
  }

  toggleTagNames(val) {
    if (this.screenWidth > 1000) {
      if (this.showVideo) return;

      this.showTagNames = val;

      this.flexGrow = val ? "3" : "0";
      this.maxWidth = val ? "550px" : "400px";

      if (!val) this.showVideo = false;
    }
  }

  invertColor(hex) {
    return invertColorCode(hex);
  }
}

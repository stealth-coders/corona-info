import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HeaderComponent } from "./components/header/header/header.component";
import { IndexPageComponent } from "./components/pages/index/index-page/index-page.component";
import { AboutPageComponent } from "./components/pages/about/about-page/about-page.component";
import { RegisterPageComponent } from "./components/pages/register/register-page/register-page.component";
import { ImpressumPageComponent } from './components/pages/info/impressum-page/impressum-page.component';
import { DataprivacyPageComponent } from './components/pages/info/dataprivacy-page/dataprivacy-page.component';

const routes: Routes = [
  { path: "header", component: HeaderComponent },
  { path: "register", component: RegisterPageComponent },
  { path: "about", component: AboutPageComponent },
  { path: "impressum", component: ImpressumPageComponent },
  { path: "dataprivacy", component: DataprivacyPageComponent },
  { path: "", component: IndexPageComponent }
];

@NgModule({
   imports: [
      RouterModule.forRoot(routes)
   ],
   exports: [
      RouterModule
   ]
})
export class AppRoutingModule {}

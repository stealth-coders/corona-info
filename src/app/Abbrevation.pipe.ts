import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'abbrevation'
})
export class AbbrevationPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    switch (value.toLowerCase()) {
      case "app_android":
        return "Android App"
      case "app_ios":
        return "iOS App";
      case "app_both":
        return "Mobile App"
      case "website":
        return "Website";
      case "app_android_website":
        return "App (Android) | Website";
      case "app_ios_website":
        return "App (iOS) | Website";
      case "app_both_website":
        return "App | Website";
    }
    return "<missing abbrevation>";
  }

}
